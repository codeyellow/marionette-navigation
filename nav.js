define(function (require) {
    'use strict';

    var Marionette = require('marionette'),
        Backbone = require('backbone');

    return Marionette.ItemView.extend({
        initialize: function () {
            this.listenTo(Backbone.history, 'all', function () {
                var url = Backbone.history.location.hash.slice(1).split('/')[0];

                this.$('.active').removeClass('active');

                if (url) {
                    this
                        .$('a[href^="#' + url + '"]')
                        .closest('li')
                        .addClass('active');
                }
            }.bind(this));
        }
    });
});