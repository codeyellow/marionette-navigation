define(function (require) {
    'use strict';

    var Marionette = require('marionette'),
        Backbone = require('backbone');

    return Marionette.ItemView.extend({
        initialize: function () {
            this.listenTo(Backbone.history, 'all', function () {
                this.render();
                this.$('.active').removeClass('active');
                this
                    .$('a[href="#' + Backbone.history.location.hash.slice(1).split('?')[0] + '"]')
                    .closest('li')
                    .addClass('active');

            }.bind(this));
        },
        /**
         * The subnav template is different per nav. Here we match specific url
         * routes to a certain subnav template.
         * 
         * @return {function}
         */
        getTemplate: function () {
            // Only the first 2 url segments determine the menu item.
            var menuItem = Backbone.history.location.hash.slice(1)
                .split('/')[0];

            if (this.templates[menuItem]) {
                this.$el.removeClass('empty');
                return this.templates[menuItem];
            } else {
                this.$el.addClass('empty');
                return this.templates.__EMPTY__;
            }
        }
    });
});