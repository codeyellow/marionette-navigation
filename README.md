# marionette-navigation

A commenly used navigation menu.

# Usage

Extend `nav.js` and `subNav.js`. You can define templates for subNav as follows:

```
define(function (require) {
    'use strict';

    var Marionette = require('node_modules/marionette-navigation/subNav'),
        TSubNavEmpty = require('text!./subNav/empty.html'),
        TSubNavData = require('text!./subNav/data.html'),
        TSubNavPlanning = require('text!./subNav/planning.html'),
        mUserCurrent = require('model/user/current'),
        _ = require('underscore');

    return Marionette.extend({
        className: 'sub-nav',
        templates: {
            'data': _.template(TSubNavData),
            'planning': _.template(TSubNavPlanning),
            '__EMPTY__': _.template(TSubNavEmpty),
        },
    });
});
```

Each key in `templates` maps to the first url slug:

- /data/oem/145 maps to the data template.
- /planning/17 maps to the planning template.